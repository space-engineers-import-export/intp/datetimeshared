﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    class BaseConfiguration
    {
        private static string DEFAULT_BROADCAST_CHANNEL = "INTP";
        protected static string SECTION = "INTP Configuration";
        private static string CHANNEL_KEY = "channel";

        protected MyIni ini;

        private IMyTerminalBlock block;

        public BaseConfiguration(IMyTerminalBlock block)
        {
            this.block = block;
            ini = new MyIni();
            ini.TryParse(block.CustomData);

            if (!ini.ContainsSection(SECTION))
            {
                createDefaultConfiguration();
            }
        }

        protected virtual Dictionary<string, object> defaultConfiguration()
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            defaults.Add(CHANNEL_KEY, DEFAULT_BROADCAST_CHANNEL);
            return defaults;
        }

        private void createDefaultConfiguration()
        {
            ini.AddSection(SECTION);
            var defaults = defaultConfiguration();
            defaults.Keys.ToList().ForEach(delegate (string key)
            {
                ini.Set(SECTION, key, defaults[key].ToString());
            });
            block.CustomData = ini.ToString();
        }

        public string getChannel()
        {
            return ini.Get(SECTION, CHANNEL_KEY).ToString();
        }
    }
}
