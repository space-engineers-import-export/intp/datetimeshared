﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript
{
    class DateObject
    {
        public static DateTime toDateTime(long ingameMilliseconds)
        {
            return DateTime.MinValue.AddMilliseconds(ingameMilliseconds);
        }
    }
}
